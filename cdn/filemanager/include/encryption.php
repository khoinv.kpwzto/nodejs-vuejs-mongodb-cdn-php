<?php

define('AES_METHOD', 'aes-256-cbc');

class Crypto{

    public function encrypt($message)
    {
        if (!isset($env)) {
            include getcwd().'/config/env.php';
        }
        $password = isset($env['private_key']) ? $env['private_key'] : "";
        if (OPENSSL_VERSION_NUMBER <= 268443727) {
            throw new RuntimeException('OpenSSL Version too old, vulnerability to Heartbleed');
        }
        
        $iv_size        = openssl_cipher_iv_length(AES_METHOD);
        $iv             = openssl_random_pseudo_bytes($iv_size);
        $ciphertext     = openssl_encrypt($message, AES_METHOD, $password, OPENSSL_RAW_DATA, $iv);
        $ciphertext_hex = bin2hex($ciphertext);
        $iv_hex         = bin2hex($iv);
        return "$iv_hex:$ciphertext_hex";
    }
    
    public function decrypt($ciphered)
    {
        try{
            if (!isset($env)) {
                include getcwd().'/config/env.php';
            }
            $password = isset($env['private_key']) ? $env['private_key'] : "";
            $iv_size    = openssl_cipher_iv_length(AES_METHOD);
            $data       = explode(":", $ciphered);
            if(count($data) < 2){
                return "";
            }
            $iv         = hex2bin($data[0]);
            $ciphertext = hex2bin($data[1]);
            return openssl_decrypt($ciphertext, AES_METHOD, $password, OPENSSL_RAW_DATA, $iv);
        } catch(Exception $ex){
            return "";
        }
        
    }
}

