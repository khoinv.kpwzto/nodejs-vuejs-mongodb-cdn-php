import Vue from 'vue'
import axios from 'axios'
import VueCookies from 'vue-cookies'

// Get csrfToken
const baseUrl = process.env.SYSTEM_URL;
axios.defaults.baseURL = baseUrl + "/api/";
axios.defaults.withCredentials = true;
// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    config.headers['X-CSRF-TOKEN'] = VueCookies.get('XSRF-TOKEN');
    config.headers['X-XSRF-TOKEN'] = VueCookies.get('XSRF-TOKEN');
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});
axios.defaults.headers.common['Access-Control-Allow-Origin'] = process.env.SYSTEM_URL;
Vue.prototype.$axios = axios;




