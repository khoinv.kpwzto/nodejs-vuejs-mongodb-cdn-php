"use strict";

module.exports = {
  escapeHtml: function(text) {
    if (Array.isArray(text)) {
      return "";
    }
    let map = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': "&quot;",
      "'": "&#039;"
    };
    return text.replace(/[&<>"']/g, function(m) {
      return map[m];
    });
  },
  loadScript: function(url) {
    const plugin = document.createElement("script");
    plugin.setAttribute("src", url);
    plugin.async = true;
    document.head.appendChild(plugin);
  },
  loadCss: function(url) {
    const plugin = document.createElement("link");
    plugin.setAttribute("href", url);
    plugin.setAttribute("rel", "stylesheet");
    plugin.async = true;
    document.head.appendChild(plugin);
  },
  getIntFromString: function(str) {
    var number = str.match(/\d/g, "");
    number = number.join("");
    return number;
  }
};
