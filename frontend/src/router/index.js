import Vue from "vue";
import Router from "vue-router";
import Home from "@/components/Home";
import NotFound from "@/components/NotFound";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home,
      meta: {
        title: "Home",
        metaTags: [
          {
            name: "description",
            content: "Home page"
          },
          {
            property: "og:description",
            content: "Home page"
          }
        ]
      }
    },
    {
      path: "*",
      name: "NotFound",
      component: NotFound,
      meta: {
        title: "Not found",
        metaTags: [
          {
            name: "description",
            content:
              "Sorry, Maybe the page moved or we deleted it. Or maybe you're just lost! "
          },
          {
            property: "og:description",
            content:
              "Sorry, Maybe the page moved or we deleted it. Or maybe you're just lost! "
          }
        ]
      }
    },
    {
      path: "/not-found",
      name: "NotFoundPage",
      component: NotFound,
      meta: {
        title: "Not found",
        metaTags: [
          {
            name: "description",
            content:
              "Sorry, Maybe the page moved or we deleted it. Or maybe you're just lost! "
          },
          {
            property: "og:description",
            content:
              "Sorry, Maybe the page moved or we deleted it. Or maybe you're just lost! "
          }
        ]
      }
    }
  ]
});
