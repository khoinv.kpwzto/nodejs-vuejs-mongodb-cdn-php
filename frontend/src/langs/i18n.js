import Vue from 'vue'
import VueI18n from 'vue-i18n'
import vnMessage from './vi.json'
import enMessage from './en.json'
import VueCookies from 'vue-cookies'
const moment = require('moment')

Vue.use(VueI18n);
Vue.use(VueCookies);

const messages = {
  vi: vnMessage,
  en: enMessage,
}

let clientLang = VueCookies.get('vue-client-lang') ? VueCookies.get('vue-client-lang') : "en";
moment.locale(clientLang);
const i18n = new VueI18n({
  locale: clientLang, // set locale
  messages,
  fallbackLocale: 'en',
})

export default i18n