// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from "vue";
import App from "./App";
import router from "./router";
import "../static/js/template";
import "../static/js/custom";
import i18n from "./langs/i18n";
import store from "./store";
import "./csrf";
import "axios-progress-bar/dist/nprogress.css";
import ConfigRouter from "./router/config";
import "./config";

ConfigRouter(router);
/* eslint-disable no-new */
export const app = new Vue({
  el: "#app",
  i18n,
  store,
  router,
  components: { App },
  template: "<App/>"
});
