import VueCookies from "vue-cookie";
import Vue from "vue";
import * as helper from "./helper";
import JQuery from "jquery";
import Paginate from "vuejs-paginate";
import { loadProgressBar as AxiosLoading } from "axios-progress-bar";
import VueMeta from "vue-meta";

window.$ = JQuery;
window.JQuery = JQuery;

const moment = require("moment");
Vue.use(require("vue-moment"), {
  moment
});
Vue.use(VueMeta);
Vue.use(VueCookies);
Vue.config.productionTip = false;
Vue.prototype._currentURL = function() {
  return window.location.href;
};
Vue.prototype._baseURL = process.env.APP_URL;
Vue.prototype._helper = helper;
Vue.prototype._moment = moment;
Vue.prototype._loadScript = helper.loadScript;
Vue.prototype._loadCss = helper.loadCss;

Vue.component("paginate", Paginate);
AxiosLoading();
