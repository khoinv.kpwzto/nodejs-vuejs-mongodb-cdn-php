import Vue from 'vue'
import Vuex from 'vuex'
import { app } from './main'
import VueCookies from 'vue-cookies'

Vue.use(Vuex)
Vue.use(VueCookies)

export default new Vuex.Store({
    state: {
        users: []
    },
    mutations: {
        SET_LANG(state, payload) {
            app.$i18n.locale = payload,
                VueCookies.set('vue-client-lang', payload)
        },
        SET_USER(state, payload) {
            state.users = payload;
        }
    },
    actions: {
        setLang({ commit }, payload) {
            commit('SET_LANG', payload)
        },
        setUser({ commit }, payload) {
            commit('SET_USER', payload)
        }
    },
    getters: {
        users: state => {
            return state.users
        }
    }
})