'use strict'
const merge = require('webpack-merge')
const devEnv = require('./dev.env')

module.exports = merge(devEnv, {
  NODE_ENV: '"testing"',
  SYSTEM_URL: '"http://localhost:3000"',
  CDN_URL: '"http://localhost:5000"',
  APP_URL : '"http://localhost:8080"'
})
