'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SYSTEM_URL: '"https://system.kemeno.net"',
  CDN_URL: '"https://cdn.kemeno.net"',
  APP_URL : '"https://kemeno.net"'
}
