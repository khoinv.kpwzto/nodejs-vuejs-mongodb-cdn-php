'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  SYSTEM_URL: '"http://localhost:3000"',
  CDN_URL: '"http://localhost:5000"',
  APP_URL : '"http://localhost:8080"'
})
