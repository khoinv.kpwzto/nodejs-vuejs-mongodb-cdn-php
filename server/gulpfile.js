const { src, dest,  watch, series } = require('gulp');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify-es').default;
const rename = require('gulp-rename');
const plumber = require('gulp-plumber');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');

function javascript(cb) {
  // body omitted
  cb();
  return src('resources/js/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ['@babel/preset-env'] }))
    .pipe(src('vendor/*.js'))
    .pipe(dest('public/js'))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(sourcemaps.write())
    .pipe(dest('public/js'));
}

function libJsConcat(cb) {
  // body omitted
  cb();
  return src([
      'public/template/admin/assets/js/modernizr.min.js', 
      'public/template/admin/assets/js/jquery.min.js', 
      'public/template/admin/assets/js/bootstrap.min.js',
      'public/template/admin/assets/js/detect.js',
      'public/template/admin/assets/js/fastclick.js',
      'public/template/admin/assets/js/jquery.slimscroll.js',
      'public/template/admin/assets/js/jquery.blockUI.js',
      'public/template/admin/assets/js/waves.js',
      'public/template/admin/assets/js/jquery.nicescroll.js',
      'public/template/admin/assets/js/jquery.scrollTo.min.js',
      'public/template/admin/assets/plugins/moment/moment.js',
      'public/template/admin/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js',
      'public/template/admin/assets/plugins/peity/jquery.peity.min.js',
      'public/template/admin/assets/js/jquery.core.js',
      'public/template/admin/assets/js/jquery.app.js',
      'public/js/init.min.js',
      'public/js/helpers.min.js'
    ],{ allowEmpty: true })
    .pipe(sourcemaps.init())
    .pipe(concat('lib.js'))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(sourcemaps.write())
    .pipe(dest('public/js'));
}

function css(cb) {
  // body omitted
  cb();
  return src("resources/css/*.scss")
    .pipe(plumber())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({ suffix: ".min" }))
    .pipe(dest("public/css"));
}

function libCssConcat(cb) {
  // body omitted
  cb();
  return src([
    "public/template/admin/assets/css/bootstrap.min.css",
    "public/template/admin/assets/css/core.css",
    "public/template/admin/assets/css/components.css",
    "public/template/admin/assets/css/icons.css",
    "public/template/admin/assets/css/pages.css",
    "public/template/admin/assets/css/responsive.css",
    "public/template/admin/assets/plugins/bootstrap-sweetalert/sweet-alert.css",
    "public/css/custom.min.css"
  ],{ allowEmpty: true })
    .pipe(sourcemaps.init())
    .pipe(autoprefixer('last 2 version', 'ie 8', 'ie 9'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('lib.min.css'))
    .pipe(sourcemaps.write())
    .pipe(dest('public/css'));
}

exports.default = function() {
  // You can use a single task
  watch('resources/css/*.scss', series(css, libCssConcat));
  // Or a composed task
  watch('resources/js/*.js', series(javascript, libJsConcat));
};

