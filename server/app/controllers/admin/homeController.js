'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const { wrap: async } = require('co');
const User = mongoose.model('User');

/**
 * Load
 */

exports.load = async(function*(req, res, next, _id) {
  const criteria = { _id };
  try {
    req.profile = yield User.load({ criteria });
    if (!req.profile) return next(new Error('User not found'));
  } catch (err) {
    return next(err);
  }
  next();
});

/**
 *  Admin Dashboard
 */

exports.dashboard = function(req, res) {
  
  res.render('admin/dashboard/index', {
    title: "Admin Dashboard"
  });
};

exports.accessDenied = function(req, res) {
  const homeUrl = process.env.HOME_URL;
  res.render('403', {
    homeUrl: homeUrl,
    title: "Access Denied"
  });
};


