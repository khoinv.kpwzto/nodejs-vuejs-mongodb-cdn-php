'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const { wrap: async } = require('co');
const only = require('only');
const User = mongoose.model('User');
const uuid = require('uuid');
require('passport-remember-me').Strategy;

/**
 * Load
 */

exports.load = async(function*(req, res, next, _id) {
  const criteria = { _id };
  try {
    req.profile = yield User.load({ criteria });
    if (!req.profile) return next(new Error('User not found'));
  } catch (err) {
    return next(err);
  }
  next();
});

/**
 * Create user
 */

exports.create = async(function*(req, res) {
  const user = new User(only(req.body, 'name email password password_confirm'));
  user.provider = 'local';
  try {
    yield user.save();
    req.logIn(user, err => {
      if (err) res.status(403).json({ 'status' : false,'message': 'Sorry! We are not able to log you in!'});
      res.status(200).json({ 'status' : true });
    });
  } catch (err) {
    const errors = Object.keys(err.errors).map(
      field => err.errors[field].message
    );
    res.status(400).json({ 'status' : false, errors: errors });
  }
});

/**
 *  Show profile
 */

exports.profile = async(function*(req, res, next) {
  const criteria = req.user.id; 
  try {
    let profile = yield User.findById(criteria,'name email');
    return res.status(200).json({ status: true, profile: profile});
  } catch (err) {
    return res.status(404).json({ status: false});
  }
});


/**
 * Auth callback
 */

exports.authCallback = login;

/**
 * Show login form
 */

exports.login = function(req, res) {
  res.render('users/login', {
    title: 'Login'
  });
};

/**
 * Show sign up form
 */

exports.signup = function(req, res) {
  res.render('users/signup', {
    title: 'Sign up',
    user: new User()
  });
};

/**
 * Logout
 */

exports.logout = function(req, res) {
  req.logout();
  res.cookie('remember_me' , "", { httpOnly: true, maxAge : null });
  return res.status(200).json({ status: true});
};

/**
 * Session
 */

exports.session = login;

/**
 * Login
 */

function login(req, res) {
  let redirectTo = "";
  if(req.session.passport.user.isAdmin){
    redirectTo = process.env.APP_URL + "/admin";
  }
  delete req.session.returnTo;
  res.json({'status': true, 'redirect_to' : redirectTo});
}

/**
 * Remember Me
 */

 exports.rememberMe = function(req, res, next){
  if (!req.body.remember_me) { return next(); }
  var token = uuid.v4();
  User.findOneAndUpdate({_id: req.user.id},{ authToken: token }, function(err){
    if(err) return res.status(403).json({ status: false});
    res.cookie('remember_me' , token, { httpOnly: true, maxAge : 30 * 24 * 60 * 60 * 1000 });
    return next();
  });
 }