const pug = require('pug');
const { wrap: async } = require('co');
const mongoose = require('mongoose');

exports.errors = function(err, res) {
    const errors = Object.keys(err.errors).map(
        field => err.errors[field].message
    );
    let html = pug.renderFile(__basedir + '/app/views/errors.pug',{__: res.__, errors: errors });
    return html;
}

exports.customError = function(message, res) {
    let html = pug.renderFile(__basedir + '/app/views/errors.pug',{__: res.__, errors: [message] });
    return html;
}

exports.toTableData = async(function* (modelName, options) {
    const Model = mongoose.model(modelName);
    const data = yield Model.list(options);
    const count = yield Model.countDocuments(options.query);
    return {
        currentPage: options.page + 1,
        lastPage: Math.ceil(count / options.limit),
        perPage: options.limit,
        total: count,
        rows: data
    };
});

exports.queryToOptions = function(query, searchFields) {
    const page = (query.page > 0 ? query.page : 1) - 1;
    let limit = query.limit && parseInt(query.limit) <= 100 ? parseInt(query.limit) : 15;
    const order = query.order && query.order == 'asc' ? 1 : -1;
    const sortField = query.sort || '_id';
    let sort = {};
    sort[sortField] = order
    
    let options = {
        limit: limit,
        page: page,
        sort: sort
    };
    options.query = {};
    if(Array.isArray(searchFields) && searchFields.length > 0){
        searchFields.forEach(function(obj){
            switch(obj.type){
                case 'like':
                    options.query[obj.key] = new RegExp(obj.value, 'i');
                    break;
                case '=':
                    options.query[obj.key] = obj.value
                    break;
            }

        })
    }
    return options;
}



exports.matchingEntities = function(select,reqBody) {
    let result = {};
    if(!reqBody || !select){
        return {};
    }
    if(typeof select === "object"){
        select.forEach(function(fieldName) {
            if( typeof reqBody[fieldName] !== 'undefined'){
                result[fieldName] = reqBody[fieldName];
            }
        });
    }
    if(typeof select === "string"){
        if( typeof reqBody[select] !== 'undefined'){
            result[select] = reqBody[select];
        }
    }
    return result;

}
// String length default: 8
// String case default: 1
// 1: String To Lower
// 2: String To Upper
// 3: Both of case
exports.makeRandomId = function(length = 8, stringCase = 1) {
    var id           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        id += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    switch(stringCase){
        case 1:
            id = id.toLowerCase();
            break;
        case 2:
            id = id.toUpperCase();
            break;
    }
    return id;
}

exports.makeVideoUrl = function(videoName){
    let url  = {
        1080: process.env.VIDEO_URL+ "/" + videoName + "/dash/1080/stream.mpd",
        720: process.env.VIDEO_URL+ "/" + videoName + "/dash/720/stream.mpd",
        480: process.env.VIDEO_URL+ "/" + videoName + "/dash/480/stream.mpd",
    };
    return url;
}
