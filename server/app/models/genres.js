'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


/**
 * Genres Schema
 */

const GenresSchema = new Schema({
  name: { type: String, default: '', trim: true, maxlength: 256 },
  color: { type: String, default: '', trim: true, maxlength: 15 },
  createdAt: { type: Date, default: Date.now }
});

/**
 * Validations
 */

GenresSchema.path('name').required(true, 'Genres name cannot be blank');
GenresSchema.path('color').required(true, 'Genres color cannot be blank');


// Methods

GenresSchema.methods = {
  createOrUpdate: function() {
    const err = this.validateSync();
    if (err && err.toString()) throw new Error(err.toString());
    return this.save();
  },
};
/**
 * Pre-remove hook
 */

GenresSchema.pre('remove', function(next) {
  // const imager = new Imager(imagerConfig, 'S3');
  // const files = this.image.files;

  // if there are files associated with the item, remove from the cloud too
  // imager.remove(files, function (err) {
  //   if (err) return next(err);
  // }, 'article');

  next();
});

/**
 * Statics
 */

GenresSchema.statics = {
  /**
   * Find article by id
   *
   * @param {ObjectId} id
   * @api private
   */

  load: function(_id) {
    return this.findOne({ _id })
      .exec();
  },

  /**
   * List articles
   *
   * @param {Object} options
   * @api private
   */

  list: function(options) {
    const criteria = options.criteria || {};
    const page = options.page || 0;
    const limit = options.limit || 30;
    return this.find(criteria)
      .sort({ createdAt: -1 })
      .limit(limit)
      .skip(limit * page)
      .exec();
  }
};

mongoose.model('Genres', GenresSchema);
