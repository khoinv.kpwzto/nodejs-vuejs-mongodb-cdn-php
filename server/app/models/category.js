'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const castError = require('../helpers/castError');
const Schema = mongoose.Schema;
mongoose.set('useFindAndModify', false);

/**
 * Home Category Schema
 */

const CategorySchema = new Schema({
  title: { type: String, default: '', trim: true, maxlength: 512 },
  status: { type: Number, default: 0 },
  type: { type: Number, default: 0 }, // 0: all, 1: news, 2: movies
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

/**
 * Virtuals
 */


/**
 * Validations
 */

CategorySchema.path('title').required(true, 'Title cannot be blank');
CategorySchema.path('status').required(true, 'Status cannot be blank');
CategorySchema.path('type').required(true, 'Type cannot be blank');


/**
 * Pre-update hook
 */

CategorySchema.pre('findOneAndUpdate', function (next) {
  this._update.updatedAt = Date.now();
  next();
});

/**
 * Methods
 */

CategorySchema.methods = {

};

/**
 * Statics
 */

CategorySchema.statics = {

  /**
   * List category
   *
   * @param {Object} options
   * @api private
   */

  list: function (options) {
    return this.find(options.query, 'title status type createdAt updatedAt')
      .limit(options.limit)
      .skip(options.limit * options.page)
      .sort(options.sort)
      .exec();
  }
};
CategorySchema.plugin(castError);

mongoose.model('Category', CategorySchema);
