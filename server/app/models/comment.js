'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * Comments Schema
 */

const CommentsSchema = new Schema({
    name: { type: String, default: '', trim: true, maxlength: 1000 },
    email: { type: String, default: '', trim: true, maxlength: 1000 },
    body: { type: String, default: '', trim: true, maxlength: 1000 },
    itemId: {
        type: Schema.ObjectId
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

/**
 * Validations
 */

CommentsSchema.path('name').required(true, 'Comment title cannot be blank');
CommentsSchema.path('email').required(true, 'Comment cannot be blank');
CommentsSchema.path('body').required(true, 'Body cannot be blank');

/**
 * Pre-remove hook
 */

CommentsSchema.pre('remove', function (next) {
    next();
});



/**
 * Methods
 */

CommentsSchema.methods = {
    /**
     * Save Comments
     *
     * @api private
     */

    createOrUpdate: function () {
        const err = this.validateSync();
        if (err && err.toString()) throw new Error(err.toString());
        return this.save();
    },
};

/**
 * Statics
 */

CommentsSchema.statics = {
    /**
     * Find comment by id
     *
     * @param {ObjectId} id
     * @api private
     */

    load: function (_id) {
        return this.findOne({ _id })
            .exec();
    },

    /**
     * List Comments
     *
     * @param {Object} options
     * @api private
     */

    list: function (options, query, select) {
        return this.find(query, select)
        .populate('user', 'name email')
        .limit(options.limit)
        .skip(options.limit * options.page)
        .sort({updatedAt: -1})
        .exec();
    }
};

mongoose.model('Comment', CommentsSchema);
