'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const crypto = require('crypto');

const Schema = mongoose.Schema;
mongoose.set('useFindAndModify', false);

/**
 * Admin Schema
 */

const AdminSchema = new Schema({
  name: { type: String, default: '' },
  email: { type: String, default: '' },
  hashed_password: { type: String, default: '' },
  salt: { type: String, default: '' },
  type: { type: Number, default: 0 },
  status: { type: Number, default: 0 },
  authToken: { type: String, default: '' }
});

const validatePresenceOf = value => value && value.length;

/**
 * Virtuals
 */

AdminSchema.virtual('password')
  .set(function(password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function() {
    return this._password;
  });

  AdminSchema.virtual('password_confirm')
  .set(function(password) {
    this._password_confirm = password;
  })
  .get(function() {
    return this._password_confirm;
  });

/**
 * Validations
 */

// the below 5 validations only apply if you are signing up traditionally

AdminSchema.path('name').validate(function(name) {
  if (this.skipValidation()) return true;
  return name.length;
}, "user.name.required");

AdminSchema.path('email').validate(function(email) {
  if (this.skipValidation()) return true;
  return email.length;
}, 'user.email.required');

AdminSchema.path('email').validate(function(email) {
  const regex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  return email.length && regex.test(email);
}, 'user.email.invalid');

AdminSchema.path('email').validate(function(email) {
  return new Promise(resolve => {
    const Admin = mongoose.model('Admin');
    if (this.skipValidation()) return resolve(true);
    
    // Check only when it is a new user or when email field is modified
    if (this.isNew || this.isModified('email')) {
        Admin.find({ email }).exec((err, users) => resolve(!err && !users.length));
    } else resolve(true);
  });
}, 'user.email.exist');

AdminSchema.path('hashed_password').validate(function(hashed_password) {
  if (this.skipValidation()) return true;
  if (this._password !== this._password_confirm) {
    this.invalidate('password_confirm', 'user.password_confirm.not_match');
  }
  return hashed_password.length && this._password.length;
}, 'user.password.required');

/**
 * Pre-save hook
 */

AdminSchema.pre('save', function(next) {
  if (!this.isNew) return next();

  if (!validatePresenceOf(this.password) && !this.skipValidation()) {
    next(new Error('user.password.invalid'));
  } else {
    next();
  }
});

/**
 * Methods
 */

AdminSchema.methods = {
  /**
   * Authenticate - check if the passwords are the same
   *
   * @param {String} plainText
   * @return {Boolean}
   * @api public
   */

  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashed_password;
  },

  /**
   * Make salt
   *
   * @return {String}
   * @api public
   */

  makeSalt: function() {
    return  process.env.APP_KEY + Math.round(new Date().valueOf() * Math.random());
  },

  /**
   * Encrypt password
   *
   * @param {String} password
   * @return {String}
   * @api public
   */

  encryptPassword: function(password) {
    if (!password) return '';
    try {
      return crypto
        .createHmac('sha1', this.salt)
        .update(password)
        .digest('hex');
    } catch (err) {
      return '';
    }
  },

  /**
   * Validation is not required if using OAuth
   */

  skipValidation: function() {
    return ~oAuthTypes.indexOf(this.provider);
  }
};

/**
 * Statics
 */

AdminSchema.statics = {
  /**
   * Load
   *
   * @param {Object} options
   * @param {Function} cb
   * @api private
   */

  load: function(options, cb) {
    options.select = options.select || 'name';
    return this.findOne(options.criteria)
      .select(options.select)
      .exec(cb);
  }
};

mongoose.model('Admin', AdminSchema);
