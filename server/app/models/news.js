'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const castError = require('../helpers/castError');
const helper = require('../helpers');
const Schema = mongoose.Schema;
mongoose.set('useFindAndModify', false);

const getTags = tags => tags.join(',');
const setTags = tags => tags.split(',').slice(0, 10); // max tags

/**
 * Home News Schema
 */

const NewsSchema = new Schema({
    title: { type: String, default: '', trim: true, maxlength: 512 },
    slug: { type: String, default: '', trim: true, maxlength: 512 },
    description: { type: String, default: '', maxlength: 1000 },
    content: { type: String, default: ''},
    categories: [
        {
            type: Schema.ObjectId,
            ref: 'Category'
        }
    ],
    tags: { type: [], get: getTags, set: setTags },
    comments: [
        {
            type: Schema.ObjectId,
            ref: 'Comment'
        }
    ],
    image: { type: String, trim: true, default: '', maxlength: 1000 },
    visit: { type: Number, default: 0 },
    status: { type: Number, default: 0 },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

/**
 * Virtuals
 */


/**
 * Validations
 */

NewsSchema.path('title').required(true, 'Title cannot be blank');
NewsSchema.path('slug').required(true, 'Slug cannot be blank');

/**
 * Pre-update hook
 */

NewsSchema.pre('findOneAndUpdate', function (next) {
    this._update.updatedAt = Date.now();
    next();
});

NewsSchema.pre('save', function (next) {
    if (!this.isNew) return next();
    this.slug = this.slug + "-" + helper.makeRandomId();
    next();
});

/**
 * Methods
 */

NewsSchema.methods = {

};

/**
 * Statics
 */

NewsSchema.statics = {

    /**
     * List news
     *
     * @param {Object} options
     * @api private
     */
    list: function (options) {
        const criteria = options.criteria || {};
        return this.find(criteria)
            .limit(options.limit)
            .skip(options.limit * options.page)
            .sort(options.sort)
            .exec();
    },
};
NewsSchema.plugin(castError);

mongoose.model('News', NewsSchema);
