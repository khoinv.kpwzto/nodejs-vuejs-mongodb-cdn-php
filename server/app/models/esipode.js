'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;
const helper = require('../helpers');

/**
 * Esipodes Schema
 */

const EsipodesSchema = new Schema({
    title: { type: String, default: '', trim: true, maxlength: 1000 },
    videoFile: { type: String, default: '', trim: true, maxlength: 1000 },
    esipodeNo: { type: Number, default: 1 },
    status: { type: Number, default: 0 },
    movie: {
        type: Schema.ObjectId,
        ref: 'Movie'
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

/**
 * Validations
 */

EsipodesSchema.path('videoFile').required(true, 'Esipode video name cannot be blank');

/**
 * Pre-remove hook
 */

EsipodesSchema.pre('remove', function (next) {
    next();
});

// On save event
EsipodesSchema.pre('save', function (next) {
    next();
});

/**
 * Methods
 */

EsipodesSchema.methods = {

    /**
     * Save esipodes
     *
     * @api private
     */

    createOrUpdate: function () {
        const err = this.validateSync();
        if (err && err.toString()) throw new Error(err.toString());
        return this.save();
    },
};

/**
 * Statics
 */

EsipodesSchema.statics = {
    /**
     * Find esipode by id
     *
     * @param {ObjectId} id
     * @api private
     */

    load: function (_id) {
        return this.findOne({ _id })
            .exec();
    },

    /**
     * List esipodes
     *
     * @param {Object} options
     * @api private
     */

    list: function (options) {
        return this.find(options.query)
            .populate('movie', 'title slug image')
            .limit(options.limit)
            .skip(options.limit * options.page)
            .sort(options.sort)
            .exec();

    }
};

mongoose.model('Esipode', EsipodesSchema);
