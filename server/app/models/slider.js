'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');
const castError = require('../helpers/castError');
const Schema = mongoose.Schema;
mongoose.set('useFindAndModify', false);

/**
 * Home Slider Schema
 */

const SliderSchema = new Schema({
  name: { type: String, default: '', trim: true, maxlength: 512 },
  movie: {
    type: Schema.ObjectId,
    ref: 'Movie'
  },
  image: { type: String, default: '', maxlength: 1000 },
  orderNo: { type: Number, default: 0 },
  status: { type: Number, default: 0 },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

/**
 * Virtuals
 */


/**
 * Validations
 */

SliderSchema.path('name').required(true, 'Name cannot be blank');
SliderSchema.path('image').required(true, 'Image cannot be blank');
SliderSchema.path('movie').required(true, 'Movie cannot be blank');

SliderSchema.path('movie').validate(function (movieId) {
  return new Promise(resolve => {
    const Movie = mongoose.model('Movie');
    if (!movieId) {
      return resolve(false);
    }
    Movie.findById(movieId).exec((err, res) => {
      return resolve(res);
    });
  });
}, 'This movie is not exist');


/**
 * Pre-update hook
 */

SliderSchema.pre('findOneAndUpdate', function(next) {
  this._update.updatedAt = Date.now();
  next();
});

/**
 * Methods
 */

SliderSchema.methods = {

};

/**
 * Statics
 */

SliderSchema.statics = {

  /**
   * List slider
   *
   * @param {Object} options
   * @api private
   */

  list: function (options) {
    const criteria = options.criteria || {};
    return this.find(criteria,'name image orderNo status createdAt updatedAt')
      .sort({ orderNo: 1 })
      .exec();
  }
};
SliderSchema.plugin(castError);

mongoose.model('Slider', SliderSchema);
