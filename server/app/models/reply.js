'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

/**
 * Replies Schema
 */

const RepliesSchema = new Schema({
    name: { type: String, default: '', trim: true, maxlength: 1000 },
    email: { type: String, default: '', trim: true, maxlength: 1000 },
    body: { type: String, default: '', trim: true, maxlength: 1000 },
    commentId: {
        type: Schema.ObjectId
    },
    user: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

/**
 * Validations
 */

RepliesSchema.path('name').required(true, 'Reply title cannot be blank');
RepliesSchema.path('email').required(true, 'Reply cannot be blank');
RepliesSchema.path('body').required(true, 'Body cannot be blank');
RepliesSchema.path('commentId').required(true, 'CommentId cannot be blank');

/**
 * Pre-remove hook
 */

RepliesSchema.pre('remove', function (next) {
    next();
});



/**
 * Methods
 */

RepliesSchema.methods = {
    /**
     * Save Replies
     *
     * @api private
     */

    createOrUpdate: function () {
        const err = this.validateSync();
        if (err && err.toString()) throw new Error(err.toString());
        return this.save();
    },
};

/**
 * Statics
 */

RepliesSchema.statics = {
    /**
     * Find reply by id
     *
     * @param {ObjectId} id
     * @api private
     */

    load: function (_id) {
        return this.findOne({ _id })
            .exec();
    },

    /**
     * List Replies
     *
     * @param {Object} options
     * @api private
     */

    list: function (options, query, select) {
        return this.find(query, select)
        .populate('user', 'name email')
        .limit(options.limit)
        .skip(options.limit * options.page)
        .sort({updatedAt: -1})
        .exec();
    }
};

mongoose.model('Reply', RepliesSchema);
