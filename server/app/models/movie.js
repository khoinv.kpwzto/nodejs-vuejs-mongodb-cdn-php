'use strict';

/**
 * Module dependencies.
 */

const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const SchemaTypes = mongoose.Schema.Types;
const helper = require('../helpers');

const getTags = tags => tags.join(',');
const setTags = tags => tags.split(',').slice(0, 10); // max tags

/**
 * Movies Schema
 */

const MoviesSchema = new Schema({
  title: { type: String, default: '', trim: true, maxlength: 1000 },
  slug: { type: String, default: '', trim: true, maxlength: 1000 },
  genres: [
    {
      type: Schema.ObjectId,
      ref: 'Genres'
    }
  ],
  esipodeNum: { type: Number, default: 1 },
  vote: { type: SchemaTypes.Decimal128, default: 0.0 },
  image: { type: String, default: '', maxlength: 1000 },
  status: { type: Number, default: 0 },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

/**
 * Validations
 */

MoviesSchema.path('title').required(true, 'Movie title cannot be blank');
MoviesSchema.path('image').required(true, 'Movie image cannot be blank');
MoviesSchema.path('genres').validate(function (genres) {
  return new Promise(resolve => {
    const Genres = mongoose.model('Genres');
    if (!genres.length) {
      return resolve(false);
    }
    genres.forEach(element => {
      Genres.findById(element).exec((err, res) => {
        return resolve(res);
      });
    });
  });
}, 'Genres can not be blank');
MoviesSchema.path('slug').required(true, 'Slug can not be blank');
MoviesSchema.path('esipodeNum').required(true, 'Esipode num can not be blank');

/**
 * Pre-remove hook
 */

MoviesSchema.pre('remove', function (next) {
  next();
});

MoviesSchema.pre('save', function (next) {
  if (!this.isNew) return next();
  this.slug = this.slug + "-" + helper.makeRandomId();
  next();
});

/**
 * Methods
 */

MoviesSchema.methods = {

  /**
   * Save movies
   *
   * @api private
   */

  createOrUpdate: function () {
    const err = this.validateSync();
    if (err && err.toString()) throw new Error(err.toString());
    return this.save();
  },
};

/**
 * Statics
 */

MoviesSchema.statics = {
  /**
   * Find movie by id
   *
   * @param {ObjectId} id
   * @api private
   */

  load: function (_id) {
    return this.findOne({ _id })
      .exec();
  },

  /**
   * List movies
   *
   * @param {Object} options
   * @api private
   */

  list: function (options) {
    return this.find(options.query)
      .populate('user', 'name email')
      .limit(options.limit)
      .skip(options.limit * options.page)
      .sort(options.sort)
      .exec();

  }
};

mongoose.model('Movie', MoviesSchema);
