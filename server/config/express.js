'use strict';

/**
 * Module dependencies.
 */

const express = require('express');
const i18n = require('i18n');
const session = require('express-session');
const compression = require('compression');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const csrf = require('csurf');
const cors = require('cors');
const helmet = require('helmet');
const upload = require('multer')();

const mongoStore = require('connect-mongo')(session);
const flash = require('connect-flash');
const winston = require('winston');
const helpers = require('view-helpers');
const ultimatePagination = require('ultimate-pagination');
const requireHttps = require('./middlewares/require-https');
const config = require('./');
const pkg = require('../package.json');
const moment = require('moment');

const env = process.env.NODE_ENV || 'development';

/**
 * Expose
 */

module.exports = function(app, passport) {

  app.use(helmet());
  // Run multiple port ( other 443 ) can not require https
  // app.use(requireHttps);

  // Compression middleware (should be placed before express.static)
  app.use(
    compression({
      threshold: 512
    })
  );

  app.use(
    cors({
      origin: [process.env.HOME_URL,process.env.CDN_URL,process.env.VIDEO_URL],
      optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
      credentials: true
    })
  );

  // Static files middleware
  app.use(express.static(config.root + '/public'));

  // Use winston on production
  let log = 'dev';
  if (env !== 'development') {
    log = {
      stream: {
        write: message => winston.info(message)
      }
    };
  }

  // Don't log during tests
  // Logging middleware
  if (env !== 'test') app.use(morgan(log));

  // set views path, template engine and default layout
  app.set('views', config.root + '/app/views');
  app.set('view engine', 'pug');

  // expose package.json to views
  app.use(function(req, res, next) {
    res.locals.pkg = pkg;
    res.locals.env = env;
    res.locals.moment = moment;
    res.locals.baseUrl = process.env.APP_URL + "/admin";
    res.locals.webUrl = process.env.HOME_URL;
    res.locals.__ = res.__ = function() {
      return i18n.__.apply(req, arguments);
    };
    next();
  });

  // CookieParser should be above session
  app.use(cookieParser());
  app.use(
    session({
      resave: false,
      saveUninitialized: true,
      secret: pkg.session_key,
      cookie:{
        maxAge: 24 * 60 * 60 * 1000,
        path: "/",
        httpOnly: true,
        secure: false
      },
      store: new mongoStore({
        url: config.db,
        collection: 'sessions'
      })
    })
  );
  
  // bodyParser should be above methodOverride
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  app.use(upload.single('image'));

  app.use(
    methodOverride(function(req) {
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method;
        delete req.body._method;
        return method;
      }
    })
  );

  // Init language package
  i18n.configure({
    locales:['en', 'vi','jp'],
    defaultLocale: 'en',
    autoReload: true,
    directory: __dirname + '/../locales',
    cookie: 'lang',
  });
  app.use(i18n.init);

  // use passport session
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(passport.authenticate('remember-me'));

  // connect flash for flash messages - should be declared after sessions
  app.use(flash());

  // should be declared after session and flash
  app.use(helpers(pkg.name));

  if (env !== 'test') {
    app.use(csrf({ cookie: true }));

    // This could be moved to view-helpers :-)
    app.use(function(req, res, next) {
      res.locals.csrf_token = req.csrfToken();
      res.cookie('XSRF-TOKEN', req.csrfToken(),{domain: process.env.SESSION_DOMAIN});
      res.locals.paginate = ultimatePagination.getPaginationModel;
      res.locals.user = req.user;
      next();
    });
  }

  if (env === 'development') {
    app.locals.pretty = true;
  }
};
