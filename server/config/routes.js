"use strict";

/*
 * Module dependencies.
 */
const home = require("../app/controllers/home");
const fail = {
  failureRedirect: "/login",
};

/**
 * Expose routes
 */

module.exports = function(app, passport) {
  const pauth = passport.authenticate.bind(passport);

  // Home route
  app.get("/", home.index);

  // Require user module routes
  require("./routes/user")(app, pauth);

  // Require admin module routes
  require("./routes/admin")(app, pauth);

  // app.get('/auth/twitter', pauth('twitter', fail), users.signin);
  // app.get('/auth/twitter/callback', pauth('twitter', fail), users.authCallback);
  // app.get(
  //   '/auth/google',
  //   pauth('google', {
  //     failureRedirect: '/login',
  //     scope: [
  //       'https://www.googleapis.com/auth/userinfo.profile',
  //       'https://www.googleapis.com/auth/userinfo.email'
  //     ]
  //   }),
  //   users.signin
  // );
  // app.get('/auth/google/callback', pauth('google', fail), users.authCallback);

  // Set language
  app.use("/set-language/:lang", (req, res) => {
    res.cookie("lang", req.params.lang, { maxAge: 900000 });
    res.redirect("back");
  });

  /**
   * Error handling
   */

  app.use(function(err, req, res, next) {
    // treat as 404
    if (
      err.message &&
      (~err.message.indexOf("not found") ||
        ~err.message.indexOf("Cast to ObjectId failed"))
    ) {
      return next();
    }

    console.error(err.stack);

    if (err.stack.includes("ValidationError")) {
      res.status(422).render("422", { error: err.stack });
      return;
    }

    // error page
    res.status(500).render("500", { error: err.stack });
  });

  // assume 404 since no middleware responded
  app.use(function(req, res) {
    const payload = {
      status: false,
      url: req.originalUrl,
      error: "Not found",
    };
    if (req.accepts("json")) return res.status(404).json(payload);
    res.status(404).render("404", payload);
  });
};
