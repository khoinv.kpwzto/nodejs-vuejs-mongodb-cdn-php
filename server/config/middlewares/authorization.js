"use strict";

/*
 *  Generic require login routing middleware
 */

exports.requiresLogin = function (req, res, next) {
  if (req.isAuthenticated()) return next();
  if (req.method == "GET") req.session.returnTo = req.originalUrl;
  return res
    .status(200)
    .json({ status: false, message: "You need login to access server!" });
};

/*
 *  Generic require login routing middleware admin
 */

exports.requiresLoginAdmin = function (req, res, next) {
  if (req.isAuthenticated() && req.session.passport.user.isAdmin === true)
    return next();
  if (req.method == "GET") req.session.returnTo = req.originalUrl;
  res.redirect("/admin/access-denied");
};

/*
 *  User authorization routing middleware
 */

exports.user = {
  hasAuthorization: function (req, res, next) {
    if (req.profile == undefined || req.profile.id != req.user.id) {
      return res
        .status(200)
        .json({ status: false, message: "You are not authorized" });
    }
    next();
  },
};
