"use strict";

/*
 * Module dependencies.
 */
const users = require("../../app/controllers/users/userController");
const auth = require("../middlewares/authorization");

/**
 * Route middlewares
 */
const userAuth = [auth.requiresLogin];

/**
 * Expose routes
 */

module.exports = function(app, pauth) {
  // User routes
  // Send CSRF token for session
  app.get("/api/getcsrftoken", function(req, res) {
    return res.json({ csrfToken: req.csrfToken() });
  });

  //Account
  app.post("/api/users/signup", users.create);
  app.post(
    "/api/users/login",
    pauth("local", {
      failureFlash: "Invalid email or password.",
    }),
    users.rememberMe,
    users.session
  );
  app.get("/api/users/profile", userAuth, users.profile);
  app.get("/api/users/logout", users.logout);
};
