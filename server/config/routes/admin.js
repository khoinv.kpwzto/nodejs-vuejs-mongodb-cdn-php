"use strict";

/*
 * Module dependencies.
 */

const admin = require(global.__basedir +
  "/app/controllers/admin/homeController");
const auth = require("../middlewares/authorization");

/**
 * Route middlewares
 */
const adminAuth = [auth.requiresLoginAdmin];

/**
 * Expose routes
 */

module.exports = function(app, pauth) {
  // Admin routes

  // admin routes non-auth
  app.get("/admin/access-denied", admin.accessDenied);

  // admin routes auth
  app.get("/admin", adminAuth, admin.dashboard);
};
