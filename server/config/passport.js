'use strict';

/*!
 * Module dependencies.
 */

const mongoose = require('mongoose');
const User = mongoose.model('User');

const local = require('./passport/local');
const google = require('./passport/google');
const twitter = require('./passport/twitter');
const remember = require('./passport/remember');

/**
 * Expose
 */

module.exports = function(passport) {
  // serialize sessions
  passport.serializeUser((user, cb) => cb(null, { id: user.id, isAdmin: user.type === 1 }));
  passport.deserializeUser((user, cb) =>
    User.load({ criteria: { _id: user.id } }, cb)
  );

  // use these strategies
  passport.use(local);
  passport.use(google);
  passport.use(twitter);

  // remember me
  passport.use(remember);
};
