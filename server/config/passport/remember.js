'use strict';

/**
 * Module dependencies.
 */
const mongoose = require('mongoose');
const User = mongoose.model('User');
const RememberMe = require('passport-remember-me').Strategy;
const uuid = require('uuid');

/**
 * Expose
 */

module.exports = new RememberMe(
    function(token, done) {
        User.findOne({ authToken: token}, function (err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }
            return done(null, user);
        });
    },
    function(user, done) {
        var token = uuid.v4();
        User.findOneAndUpdate({_id: user.id},{ authToken: token }, function(err){
            if(err) { return done(err); }
            return done(null, token);
        });
    }
);
