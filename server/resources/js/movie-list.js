var queryParams = function(params){
    params.page         = parseInt(params.offset / params.limit) + 1;
    return params;
}
$(document).on("change","#title",function(){
    changeSlug();
})
$(document).on("keyup","#title",function(){
    changeSlug();
})
var changeSlug = function(){
    var slug = $("#slug").val();
    if(typeof KhongDau !== 'undefined' && $("#changeSlug").prop("checked")){
        slug = convertToSlug(KhongDau($("#title").val(),['slug']));
    }
    $("#slug").val(slug);
}

$(document).on("click",".list-esipode",function(){
    var id = $(this).data("id");
    window.location.href = config.esipode_url + id;
})

var formatterAction = function(value, row, index, field){
    var content = $('<a href="javascript:void(0);" data-id="'+ row._id + '" class="btn btn-small btn-primary edit-row">'+messages.Edit+'</a>');
    var esipodeList = $('<a href="javascript:void(0);" data-id="'+ row._id + '" class="btn btn-small btn-success list-esipode mgl-5">'+messages.EsipodeList+'</a>');
    return $("<div>").append(content).append(esipodeList).html();
}