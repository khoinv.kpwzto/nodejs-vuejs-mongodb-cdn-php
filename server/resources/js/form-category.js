$(document).ready(function () {
    $.FormApp.setEventSave();
})
$(window).off('getFormData', getFormData);
$(window).on('getFormData', getFormData);

$.InitApp.setFormError();
var getFormData = function (id) {
    var res = {
        'id': id ? id : "",
        'title': $("#title").val(),
        'type': $("input[name='type']:checked").val(),
        'status': $("input[name='status']:checked").val()
    };
    return res;
}