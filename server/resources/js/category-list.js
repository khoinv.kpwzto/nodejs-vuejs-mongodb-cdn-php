var queryParams = function(params) {
  return params;
};

var formatterType = function(value, row, index, field) {
  var content;
  switch (row.type) {
    case 0:
      content = $('<span class="label label-table label-primary">All</span>');
      break;
    case 1:
      content = $('<span class="label label-table label-primary">News</span>');
      break;
    case 2:
      content = $('<span class="label label-table label-primary">Movie</span>');
      break;
  }
  return $("<div>")
    .append(content)
    .html();
};
