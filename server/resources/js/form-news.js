$(document).ready(function () {
    $.FormApp.fileManager();
    $.FormApp.setEventSave();
    $.TinyClass.init("#content", "items");
    $("#tags").tagsinput('items')
    $('#categories').select2({
        ajax: {
            url: config.url_search_categories,
            dataType: 'json',
            data: function (params) {
                var query = {
                    q: params.term
                }
                return query;
            },
            processResults: function (data) {
                var results = $.map( data.rows, function(row) {
                    return {id: row._id, text: row.title};
                });
                return {
                    results: results
                };
            }
        }
    });
})
$(window).off('getFormData', getFormData);
$(window).on('getFormData', getFormData);

$.InitApp.setFormError();
var getFormData = function (id) {
    var res = {
        'id': id ? id : "",
        'title': $("#title").val(),
        'slug': $("#slug").val(),
        'image': $("#image").val(),
        'tags': $('#tags').val(),
        'categories': $("#categories").val(),
        'description': $("#description").val(),
        'content': tinyMCE.get('content').getContent(),
        'status': $("input[name='status']:checked").val()
    };
    return res;
}