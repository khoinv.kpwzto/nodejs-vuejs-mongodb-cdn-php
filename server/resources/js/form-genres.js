$(document).ready(function () {
    $.FormApp.setEventSave();
})
$(window).off('getFormData', getFormData);
$(window).on('getFormData', getFormData);

$.InitApp.setFormError();
var getFormData = function (id) {
    var res = {
        'id': id ? id : "",
        'name': $("#name").val(),
        'color': $("#color").val()
    };
    return res;
}