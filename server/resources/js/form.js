/**
 * Form App
 */
var OnMessage = function (e) {
    var event = e.originalEvent;
    // Make sure the sender of the event is trusted
    if (event.data.sender === 'responsivefilemanager') {
        if (event.data.field_id) {
            var fieldID = event.data.field_id;
            var url = event.data.url;
            $('#' + fieldID).val(url).trigger('change');
            $('#' + fieldID).closest(".form-group").find('img.img-demo').attr('src', url);
            $.fancybox.close();
            // Delete handler of the message from ResponsiveFilemanager
            $(window).off('message', OnMessage);
        }
    }
}

!function ($) {
    "use strict";
    var FormApp = function () {
        this._btnAdd = $('#btn-add-row');
        this._btnEdit = $('.edit-row');
        this._table = '#table-list';
        this._modalElement = $('#modal-content');
        this._modalForm = $('#modal-form');
        this._btnFileManager = '.btn-filemanager';
        this._form = '#form-entity';
        this._removeImage = '.icon-delete';
        this._btnSave = '#btnSave';
    };
    FormApp.prototype._getVal = function (_id) {
        return $("#" + _id).val();
    },
        FormApp.prototype.hideModal = function () {
            $('.modal').modal('hide');
        },
        FormApp.prototype.create = function () {
            var _self = this;
            $.ajax({
                type: 'get',
                url: config.add_url,
                async: true,
                success: function (response) {
                    _self._modalElement.empty();
                    _self._modalElement.append(response);
                    _self._modalForm.modal();
                },
                error: function (exception) {
                    console.log(exception);
                }
            });
        },
        FormApp.prototype.edit = function (id) {
            var _self = this;
            $.ajax({
                type: 'get',
                url: config.edit_url + '/' + id,
                async: true,
                success: function (response) {
                    _self._modalElement.empty();
                    _self._modalElement.append(response);
                    _self._modalForm.modal();
                },
                error: function (exception) {
                    console.log(exception);
                }
            });
        },
        FormApp.prototype.setEventSave = function () {
            var self = this;
            $(self._btnSave).on("click", function () {
                self.save($(this).attr("data-pk"), $(this).attr("data-action-validate"), $(this).attr("data-type"));
            })

            $(self._removeImage).click(function () {
                var parent = $(this).closest('.form-group');
                parent.find('img.img-demo').attr('src', '/img/no_image.png');
                parent.find('.image-url-input').val("");
            });
        },
        FormApp.prototype.save = function (id) {
            var self = this;
            $(".error-content").remove();
            var dataPush = {};
            if (typeof getFormData !== "undefined") {
                dataPush = window.getFormData(id);
            }
            if (!$($(self._form)).parsley().validate()) {
                return;
            }
            var formData = new FormData();
            $.each(dataPush, function (key, value) {
                // Append array element
                if (typeof value === 'object') {
                    if (value !== null) {
                        $.each(value, function (arrKey, arrVal) {
                            formData.append(key + '[]', arrVal);
                        });
                    }
                } else {
                    formData.append(key, value);
                }
            })
            // Loading.show();
            var urlPost = typeof id == 'string' && id != '' ? config.edit_url + '/' + id : config.add_url;
            $.ajax({
                type: 'post',
                url: urlPost,
                async: true,
                dataType: "JSON",
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (response) {
                    // Loading.hide();
                    $(self._table).bootstrapTable('refresh');
                    self.hideModal();
                    swal("Successful", response.message, "success");
                },
                error: function (error) {
                    // Loading.hide();
                    // Scroll to error view
                    var elment = document.getElementById("modal-content");
                    elment.scrollIntoView({ behavior: 'smooth', block: 'start' });
                    Helpers.swalErrors(self._form, error);
                }
            });
        },
        FormApp.prototype.init = function () {
            var _self = this;
            _self._btnAdd.click(function () {
                _self.create();
                return false;
            });
        },
        FormApp.prototype.editRow = function (id) {
            var _self = this;
            _self.edit(id);
        }
    FormApp.prototype.fileManager = function (type = 1) {
        var _self = this;
        $(_self._btnFileManager).click(function () {
            $(window).on('message', OnMessage);
            var fieldId = this.getAttribute('data-field-id');
            $.fancybox.open({
                src: $.InitApp.addParam(config.cdn_url, {
                    'akey': config.cdn_token,
                    'type': type,
                    'crossdomain': 1,
                    'field_id': fieldId,
                    'lang': lang
                }),
                type: 'iframe'
            });
        });

    },
        //init FormApp
        $.FormApp = new FormApp(), $.FormApp.Constructor = FormApp

}(window.jQuery),

    //initializing form application module
    function ($) {
        "use strict";
        $.FormApp.init();
        $(document).on("click", ".edit-row", function () {
            var id = $(this).data("id");
            $.FormApp.editRow(id);
        })
    }(window.jQuery);

var TinyClass = function () {

}

TinyClass.prototype.init = function (element, folder) {
    if ($(element).length > 0) {
        tinymce.remove("textarea" + element);
        tinymce.init({
            selector: "textarea" + element,
            inline: false,
            force_br_newlines: false,
            force_p_newlines: true,
            forced_root_block: '',
            theme: "modern",
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                "save table contextmenu directionality emoticons template paste textcolor",
            ],
            toolbar: "insertfile undo redo | styleselect | fontsizeselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
            style_formats: [
                { title: 'Bold text', inline: 'b' },
                { title: 'Red text', inline: 'span', styles: { color: '#ff0000' } },
                { title: 'Red header', block: 'h1', styles: { color: '#ff0000' } },
                { title: 'Table styles' },
                { title: 'Table row 1', selector: 'tr', classes: 'tablerow1' }
            ],
            fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt 30pt 32pt 34pt 36pt 38pt',
            file_browser_callback: function (field_name, url, type, win) {
                if (typeof config !== "undefined" && typeof config.cdn_token !== "undefined") {
                    open_file_manager(config.cdn_token, field_name, 0, folder);
                }
            },
            language: lang,
            setup: function (ev) {
                ev.on('keyup', function (e) {
                    $("textarea" + element).val(ev.getContent())
                });
            }
        });
    }
},
    //init TinyClass
    $.TinyClass = new TinyClass(), $.TinyClass.Constructor = TinyClass

function open_file_manager(access_token, field_id, relative_url, folder) {
    if (typeof config === undefined) {
        return "";
    }
    if (folder == undefined) {
        folder = "";
    }
    $(window).on('message', OnMessage);
    $.fancybox.open({
        src: $.InitApp.addParam(config.cdn_url, {
            'akey': access_token,
            'type': 2,
            'crossdomain': 1,
            'field_id': field_id,
            'lang': lang,
            'relative_url': relative_url,
            'folder': folder,
        }),
        type: 'iframe'
    });
}
