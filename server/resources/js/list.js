"use_strict"

$(window).load(function() {
    $('[data-toggle="table"]').show();
 });
 
$(document).ready(function () {

    // BOOTSTRAP TABLE - LIST
    // =================================================================
    // Require Bootstrap Table
    // =================================================================
    var $table = $('#table-list'), 
        $remove = $('#btn-delete-row'), 
        $block = $('#btn-block-row'),
        $active = $('#btn-active-row'),
        $inactive = $('#btn-inactive-row'), 
        $unblock = $("#btn-unblock-row");

    $table.on('check.bs.table uncheck.bs.table check-all.bs.table uncheck-all.bs.table', function () {
        $remove.prop('disabled', !$table.bootstrapTable('getSelections').length);
        $block.prop('disabled', !$table.bootstrapTable('getSelections').length);
        $unblock.prop('disabled', !$table.bootstrapTable('getSelections').length);
        $active.prop('disabled', !$table.bootstrapTable('getSelections').length);
        $inactive.prop('disabled', !$table.bootstrapTable('getSelections').length);
    });

    $remove.click(function () {
        postDataAjax('delete');
    });
    $block.click(function () {
        postDataAjax('block');
    });
    $unblock.click(function () {
        postDataAjax('unblock');
    });
    $active.click(function () {
        postDataAjax('active');
    });
    $inactive.click(function () {
        postDataAjax('inactive');
    });
    var postDataAjax = function($type){
        var $title = $table.data('title-' + $type);
        var $message = $table.data('message-' + $type);
        var $url = $table.data('url-' + $type);
        var $successMessage = $table.data('success-message-' + $type);
        swal({
            title: $title,
            text: $message,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: messages.Cancel,
            confirmButtonClass: "btn-danger",
            confirmButtonText: messages.Yes,
            closeOnConfirm: false
        },
        function(){
            var ids = $.map($table.bootstrapTable('getSelections'), function (row) {
                return row._id
            });
            if($url == undefined || $url == ""){
                Helpers.swalErrors();
                return;
            }
            $.ajax({
                type:'POST',
                url: $url,
                async: true,
                data:{
                    multiple_id: ids
                },
                success:function(response){
                    disabledAllAction();
                    $table.bootstrapTable('refresh');
                    swal(messages.Successful, $successMessage, "success");
                },
                error: function(e){
                    Helpers.swalErrors();
                    return;
                }
            });
        });
    }
    var disabledAllAction = function(){
        $remove.prop('disabled', true);
        $block.prop('disabled', true);
        $unblock.prop('disabled', true);
        $active.prop('disabled', true);
        $inactive.prop('disabled', true);
    }
});

$('#table-list').on('load-success.bs.table', function () {
    onLoadTable();
})

var onLoadTable = function(){
    if(typeof $.fancybox !== 'undefined'){
        $(".image-fancybox").fancybox();
    }
}

$(document).on("click","#btnSearch",function(){
    $("#table-list").bootstrapTable("refresh");
});

$(document).on("click","#btnReset",function(){
    $("#formSearch").trigger('reset');
    $(this).closest('form').find("input[type=text]").val("");
    $("#table-list").bootstrapTable("refresh");
});

var queryParams = function(params){
    return params;
}

var formatterStatus = function(value, row, index, field){
    var content = value == "1" ? $('<span class="label label-table label-success">Active</span>') : $('<span class="label label-table label-danger">Hidden</span>');
    return $("<div>").append(content).html();
}

var formatterCheckbox = function(value, row, index, field){
    var content = $("<input value='" + row.id + "' />").addClass("pk-id").hide();
    return $("<div>").append(content).html();
}

var formatterOrder = function(value, row, index, field){
    var no = index + 1;
    return no;
}

var formatterImageFancybox = function(value, row, index, field){
    if(row.image != null){
        var aTag = $("<a class='image-fancybox' href='"+ row.image +"'></a>");
        var image = $('<img class="image-list" src="'+ row.image +'" />');
        aTag.append(image);
        return $("<div>").append(aTag).html();
    }
}

var formatterDate = function(value, row, index, field){
    if(!value instanceof Date){
        return "-";
    }
    // Date format Y-m-d H:i:s
    var d = new Date(Date.parse(value)),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = '' + d.getHours(),
        minute = '' + d.getMinutes(),
        second = '' + d.getSeconds();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
    if (hour.length < 2) 
        hour = '0' + hour;
    if (minute.length < 2) 
        minute = '0' + minute;
    if (second.length < 2) 
        second = '0' + second;
    return [year, month, day].join('-') + " " + [hour, minute, second].join(':');

}

var formatterAction = function(value, row, index, field){
    var content = $('<a href="javascript:void(0);" data-id="'+ row._id + '" class="btn btn-small btn-primary edit-row">'+messages.Edit+'</a>');
    return $("<div>").append(content).html();
}