"use strict";

$(document).ready(function () {
  $('#table-list tbody').sortable({
    update: function update() {
      orderTable();
    }
  });
});

var orderTable = function orderTable() {
  var $rows = $("#table-list tbody tr");
  var $sortIds = [];

  for (var i = 0; i < $rows.length; i++) {
    if ($rows[i].children[1] != undefined) {
      var no = i + 1;
      $rows[i].children[1].innerHTML = no;
      var id = $($rows[i].children[1]).closest('tr').attr('id');
      $sortIds.push(id);
    }
  }

  if ($sortIds.length > 0) {
    $.ajax({
      type: 'POST',
      url: config.url_sort,
      dataType: "JSON",
      data: {
        multiple_id: $sortIds
      },
      success: function success() {
        console.log("Sorted");
      },
      error: function error(e) {
        $("#table-list").bootstrapTable("refresh");
        Helpers.swalErrors();
      }
    });
  }
};

var queryParams = function queryParams(params) {
  return params;
};