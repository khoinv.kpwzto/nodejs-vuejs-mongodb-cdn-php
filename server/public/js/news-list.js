"use strict";

var queryParams = function queryParams(params) {
  return params;
};

$(document).on("change", "#title", function () {
  changeSlug();
});
$(document).on("keyup", "#title", function () {
  changeSlug();
});

var changeSlug = function changeSlug() {
  var slug = $("#slug").val();

  if (typeof KhongDau !== 'undefined' && $("#changeSlug").prop("checked")) {
    slug = convertToSlug(KhongDau($("#title").val(), ['slug']));
  }

  $("#slug").val(slug);
};