"use strict";

$(document).ready(function () {
  $.MultipleSelect.init("#genres", "Genres", "Genres Selected");
  $.FormApp.fileManager();
  $.FormApp.setEventSave();
});
$(window).off('getFormData', getFormData);
$(window).on('getFormData', getFormData);
$.InitApp.setFormError();

var getFormData = function getFormData(id) {
  var res = {
    'id': id ? id : "",
    'title': $("#title").val(),
    'slug': $("#slug").val(),
    'esipodeNum': $("#esipodeNum").val(),
    'genres': $("#genres").val(),
    'image': $("#image").val(),
    'status': $("input[name='status']:checked").val()
  };
  return res;
};