"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var escapeHtml = function escapeHtml(text) {
  if (Array.isArray(text)) {
    return "";
  }

  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  return text.replace(/[&<>"']/g, function (m) {
    return map[m];
  });
};
/**
 * Number.prototype.format(n, x)
 *
 * @param integer n: length of decimal
 * @param integer x: length of sections
 */


Number.prototype.format = function (n, x) {
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
  return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};
/**
 * Function: Check Json
 * @param str
 * @returns {boolean}
 * @constructor
 */


function _isJson(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }

  return true;
}

var Helpers = function () {
  var createMessage = function createMessage(errors, type) {
    var _self = this;

    if (Object.size(errors) <= 0) {
      return;
    }

    var html = $('<div class="' + type + '-content"></div>');

    if (Array.isArray(errors)) {
      $.each(errors, function (index, value) {
        var mes = $('<div class="alert-message">' + _self.escapeHtml(value) + '</div>');
        html.append(mes);
      });
    } else {
      var mes = $('<div class="alert-message">' + _self.escapeHtml(errors) + '</div>');
      html.append(mes);
    }

    return html;
  };

  var setErrors = function setErrors(formElement, errors) {
    var _self = this;

    var html = _self.createMessage(errors, 'error');

    $(formElement).empty();
    $(formElement).append(html);
  };

  var setSuccess = function setSuccess(formElement, message) {
    var _self = this;

    var html = _self.createMessage(message, 'success');

    $(formElement).empty();
    $(formElement).append(html);
  };

  var clearForm = function clearForm(formId) {
    $(formId).reset();
  };

  var swalErrors = function swalErrors(formId, errors) {
    if (!errors) {
      swal(messages.error, messages.had_some_error, 'error');
      return;
    }

    $(formId).find(".error-content").remove();
    var resHtml = "";

    if (Array.isArray(errors) && _typeof(errors.responseJSON) === undefined) {
      var _self = this;

      resHtml = _self.createMessage(errors, 'error');
    }

    if (_typeof(errors.responseJSON) !== undefined) {
      resHtml = errors.responseJSON.error;
    }

    $(formId).prepend(resHtml);
  };

  return {
    createMessage: createMessage,
    setErrors: setErrors,
    swalErrors: swalErrors,
    clearForm: clearForm
  };
}();

var convertToSlug = function convertToSlug(Text) {
  return Text.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
};