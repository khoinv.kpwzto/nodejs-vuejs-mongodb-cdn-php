"use strict";

var queryParams = function queryParams(params) {
  params.page = parseInt(params.offset / params.limit) + 1;
  return params;
};

var formatterAction = function formatterAction(value, row, index, field) {
  var content = $('<a href="javascript:void(0);" data-id="' + row._id + '" class="btn btn-small btn-primary edit-row">' + messages.Edit + '</a>');
  return $("<div>").append(content).html();
};