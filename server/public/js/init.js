"use strict";

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

setCookie = function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

var getCookie = function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');

  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];

    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }

    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }

  return "";
};

var InitApp = function InitApp() {};

InitApp.prototype.setInputFilter = function (textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function (event) {
    textbox.addEventListener(event, function () {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
  });
}, // You can only input an integer value
InitApp.prototype.numberInput = function (element) {
  this.setInputFilter(document.getElementById(element), function (value) {
    return /^\d*\.?\d*$/.test(value);
  });
}; // You can only input from 0 to 10 in float value

InitApp.prototype.voteInput = function (element) {
  this.setInputFilter(document.getElementById(element), function (value) {
    return /^-?\d*[.,]?\d{0,1}$/.test(value) && (value === "" || parseInt(value) <= 10) && (value === "" || parseInt(value) >= 0);
  });
}; // Add params to url


InitApp.prototype.addParam = function (url, params) {
  var result = new URL(url);
  $.each(params, function (key, value) {
    result.searchParams.append(key, value);
    result.searchParams.set(key, value);
  });
  return result;
}; // Set lang


InitApp.prototype.initLang = function (url, params) {
  var shortLang = getCookie('lang');
  lang = shortLang ? shortLang : 'en';
}; // Set form error


InitApp.prototype.setFormError = function () {
  $("#form-entity").parsley({
    successClass: "has-success",
    errorClass: "has-error",
    classHandler: function classHandler(el) {
      return el.$element.closest(".form-group");
    },
    errorsContainer: function errorsContainer(el) {
      return el.$element.closest(".form-group");
    },
    errorsWrapper: "<span class='help-block'></span>",
    errorTemplate: "<span></span>"
  });
}; //init InitApp


$.InitApp = new InitApp(), $.InitApp.Constructor = InitApp;
$.InitApp.initLang();

var MultipleSelect = function MultipleSelect() {};

MultipleSelect.prototype.init = function (element, selectableHeader, selectionHeader) {
  if ($(element).length > 0) {
    $(element).multiSelect({
      keepOrder: true,
      selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='" + selectableHeader + "'>",
      selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='" + selectionHeader + "'>",
      afterInit: function afterInit(ms) {
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';
        that.qs1 = $selectableSearch.quicksearch(selectableSearchString).on('keydown', function (e) {
          if (e.which === 40) {
            that.$selectableUl.focus();
            return false;
          }
        });
        that.qs2 = $selectionSearch.quicksearch(selectionSearchString).on('keydown', function (e) {
          if (e.which == 40) {
            that.$selectionUl.focus();
            return false;
          }
        });
      },
      afterSelect: function afterSelect() {
        this.qs1.cache();
        this.qs2.cache();
      },
      afterDeselect: function afterDeselect() {
        this.qs1.cache();
        this.qs2.cache();
      }
    });
  }
}, //init MultipleSelect
$.MultipleSelect = new MultipleSelect(), $.MultipleSelect.Constructor = MultipleSelect;

String.prototype.replaceAll = function (search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};