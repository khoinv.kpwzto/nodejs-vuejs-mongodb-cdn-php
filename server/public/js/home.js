"use strict";

/**
 * Login, Sign Up App
 */
!function ($) {
  "use strict";

  var LoginApp = function LoginApp() {
    this._btnLogin = $('#btnLogin'), this._btnSignUp = $('#btnSignUp');
  };

  LoginApp.prototype._getVal = function (_id) {
    return $("#" + _id).val();
  }, LoginApp.prototype._rememberMe = function () {
    return $("#rememberMe").prop("checked");
  }, LoginApp.prototype.login = function () {
    var _self = this;

    var postData = {
      'email': _self._getVal("email"),
      'password': _self._getVal("password"),
      'remember_me': _self._rememberMe()
    };
    $.ajax({
      type: 'post',
      url: "/users/login",
      dataType: "JSON",
      data: postData,
      success: function success(response) {
        console.log(response);
      },
      error: function error(exception) {
        console.log(exception);
      }
    });
  }, LoginApp.prototype.signUp = function () {
    var _self = this;

    var postData = {
      'email': _self._getVal("email-signup"),
      'name': _self._getVal("name-signup"),
      'password': _self._getVal("password-signup"),
      'password_confirm': _self._getVal("password-signup-confirm")
    };
    $.ajax({
      type: 'post',
      url: "/users/signup",
      dataType: "JSON",
      data: postData,
      success: function success(response) {
        console.log(response);

        if (!response.status) {
          $.InitApp.setErrorMessage('#error-signup', response.errors);
          return;
        }

        window.location.reload();
      },
      error: function error(exception) {
        console.log(exception);
      }
    });
  }, LoginApp.prototype.init = function () {
    var _self = this;

    _self._btnLogin.click(function () {
      _self.login();

      return false;
    });

    _self._btnSignUp.click(function () {
      _self.signUp();

      return false;
    });
  }, //init LoginApp
  $.LoginApp = new LoginApp(), $.LoginApp.Constructor = LoginApp;
}(window.jQuery), //initializing main application module
function ($) {
  "use strict";

  $.LoginApp.init();
}(window.jQuery);