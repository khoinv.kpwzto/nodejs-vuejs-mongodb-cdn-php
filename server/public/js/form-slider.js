"use strict";

$(document).ready(function () {
  $.FormApp.fileManager();
  $.FormApp.setEventSave();
  $('#movie').select2({
    maximumSelectionLength: 1,
    ajax: {
      url: config.url_search_movie,
      dataType: 'json',
      data: function data(params) {
        var query = {
          q: params.term
        };
        return query;
      },
      processResults: function processResults(data) {
        var results = $.map(data.rows, function (row) {
          return {
            id: row._id,
            text: row.title
          };
        });
        return {
          results: results
        };
      }
    }
  });
});
$(window).off('getFormData', getFormData);
$(window).on('getFormData', getFormData);

var getFormData = function getFormData(id) {
  var res = {
    'id': id ? id : "",
    'name': $("#name").val(),
    'image': $("#image").val(),
    'movie': $("#movie").val(),
    'status': $("input[name='status']:checked").val()
  };
  return res;
};