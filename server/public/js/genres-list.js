"use strict";

var formatterColor = function formatterColor(value, row, index, field) {
  var color = row.color;

  if (color == "") {
    return "-";
  }

  var content = $("<span style='color:" + color + "'>" + color + "</span>");
  return $("<div/>").append(content).html();
};