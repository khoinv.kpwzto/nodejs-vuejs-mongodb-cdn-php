This is basic project with nodejs vuejs mongo database and cdn upload

# Module

- [CDN (php)]
- [Frontend (vuejs)]
- [Server (nodejs)]

Server edit from https://github.com/madhums/node-express-mongoose
Thanks madhums

## Install

```sh
git clone https://gitlab.com/khoinv.kpwzto/nodejs-vuejs-mongodb-cdn-php.git
```

## License

MIT
